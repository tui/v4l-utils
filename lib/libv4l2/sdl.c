/* -*- c-file-style: "linux" -*- */
/* SDL support.

   Copyright 2017 Pavel Machek, LGPL
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

struct sdl {
	SDL_Window *window;
	SDL_Surface *liveview, *screen;

	int wx, wy;
	int sx, sy;
	int bx, by;
	int factor;
	float focus, gain, exposure, do_focus, do_exposure; 
};

#if 0
void loop(void) {
	int done;
	SDL_Event event;

	while(!done){ //While program isn't done                                   
		while(SDL_PollEvent(&event)){ //Poll events                        
			switch(event.type){ //Check event type                     
			case SDL_QUIT: //User hit the X (or equivelent)            
				done = true; //Make the loop end                   
				break; //We handled the event                      
			} //Finished with current event                            
		} //Done with all events for now                                   
	} //Program done, exited                                                   
}
#endif

typedef struct {
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 alpha;
} pixel;

#define d_raw 1

void sfc_put_pixel(SDL_Surface* liveview, int x, int y, pixel *p)
{
	Uint32* p_liveview = (Uint32*)liveview->pixels;
	p_liveview += y*liveview->w+x;
	*p_liveview = SDL_MapRGBA(liveview->format,p->r,p->g,p->b,p->alpha);
}

#if 0
int pix_exposure(float x)
{
	return sy*x / 199410.0;
}

int pix_gain(float x)
{
	return sy*x / 16.0;
}

int render_statistics(SDL_Surface* liveview)
{
	pixel white;
	white.r = (Uint8)0xff;
	white.g = (Uint8)0xff;
	white.b = (Uint8)0xff;
	white.alpha = (Uint8)128;

	//printf("Stats: focus %d, gain %d, exposure %d\n", focus, gain, exposure);

	for (int x=0; x<sx && x<sx*focus; x++)
		put_pixel(liveview, x, 0, &white);

	for (int y=0; y<sy && y<pix_gain(gain); y++)
		put_pixel(liveview, 0, y, &white);

	for (int y=0; y<sy && y<pix_exposure(exposure); y++)
		put_pixel(liveview, sx-1, y, &white);

	for (int x=0; x<sx; x++)
		put_pixel(liveview, x, sy-1, &white);

	return 0;
}
#endif

void sdl_begin_paint(struct sdl *m) {
	//Fill the surface white                                                   
	SDL_FillRect(m->liveview, NULL, SDL_MapRGB( m->liveview->format, 0, 0, 0 ));

	SDL_LockSurface(m->liveview);
}

void sdl_finish_paint(struct sdl *m) {
	SDL_UnlockSurface(m->liveview);
	SDL_Rect rcDest = { m->bx, m->by, m->sx, m->sy };

	SDL_BlitSurface(m->liveview, NULL, m->screen, &rcDest);
	//Update the surface                                                       
	SDL_UpdateWindowSurfaceRects(m->window, &rcDest, 1);
}
  
void sdl_paint_image(struct sdl *m, char **xpm, int x, int y) {
	SDL_Surface *image = IMG_ReadXPMFromArray(xpm);
	if (!image) {
		printf("IMG_Load: %s\n", IMG_GetError());
		exit(1);
	}

	int x_pos = x - image->w/2, y_pos = y - image->h/2;

	SDL_Rect rcDest = { x_pos, y_pos, image->w, image->h };
	int r = SDL_BlitSurface ( image, NULL, m->screen, &rcDest );

	if (r) {
		printf("Error blitting: %s\n", SDL_GetError());
		exit(1);
	}
	SDL_FreeSurface ( image );
}

void sdl_paint_ui(struct sdl *m) {
	static char *wait_xpm[] = {
		"16 9 2 1",
		"# c #ffffff",
		". c #000000",
		"....########....",
		".....#....#.....",
		".....#....#.....",
		"......#..#......",
		".......##.......",
		"......#..#......",
		".....#....#.....",
		".....#....#.....",
		"....########....",
	};

	static char *ok_xpm[] = {
		"16 9 2 1",
		"# c #ffffff",
		". c #000000",
		"...............#",
		"............###.",
		"..........##....",
		"#.......##......",
		".#.....#........",
		"..#...#.........",
		"..#..#..........",
		"...##...........",
		"...#............",
	};

	static char *exit_xpm[] = {
		"16 9 2 1",
		"x c #ffffff",
		". c #000000",
		"....x......x....",
		".....x....x.....",
		"......x..x......",
		".......xx.......",
		".......xx.......",
		"......x..x......",
		".....x....x.....",
		"....x......x....",
		"................",
	};

	static char *f1m_xpm[] = {
		"16 9 2 1",
		"# c #ffffff",
		". c #000000",
		"....##..........",
		"...#.#..........",
		"..#..#..........",
		".....#...#.#.##.",
		".....#...##.#..#",
		".....#...#..#..#",
		".....#...#..#..#",
		".....#...#..#..#",
		"................",
	};

	static char *f25cm_xpm[] = {
		"16 9 2 1",
		"x c #ffffff",
		". c #000000",
		".xxx..xxxx......",
		"x...x.x.........",
		"...x..xxx.......",
		"..x......x..xx.x",
		".x.......x.x.xxx",
		"xxxxx.xxx...xxxx",
		"................",
		"................",
		"................",
	};

	static char *iso400_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		"x..x.xxxx.xxxx..",
		"x..x.x..x.x..x..",
		"xxxx.x..x.x..x..",
		"...x.x..x.x..x..",
		"...x.xxxx.xxxx..",
		"................",
		".x..xx..x.......",
		".x.x...x.x......",
		".x..x..x.x......",
		".x...x.x.x......",
		".x.xx...x.......",
		"................",
	};

	static char *time_1_100_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		".x....x.........",
		".x...x..........",
		".x..x...........",
		".x.x............",
		"................",
		"..x.xxxx.xxxx...",
		"..x.x..x.x..x...",
		"..x.x..x.x..x...",
		"..x.x..x.x..x...",
		"..x.x..x.x..x...",
		"..x.xxxx.xxxx...",
		"................",
	};

	static char *time_1_10_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		".x....x.........",
		".x...x..........",
		".x..x...........",
		".x.x............",
		"................",
		"..x.xxxx........",
		"..x.x..x........",
		"..x.x..x........",
		"..x.x..x........",
		"..x.x..x........",
		"..x.xxxx........",
		"................",
	};

	static char *af_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		".....xxxxxxx....",
		".....x..........",
		".....x..........",
		".x...xxxxx......",
		"x.x..x..........",
		"xxx..x..........",
		"x.x..x..........",
		"x.x..x..........",
		"................",
		"................",
		"................",
		"................",
	};

	static char *ae_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		".....xxxxxxx....",
		".....x..........",
		".....x..........",
		".x...xxxxx......",
		"x.x..x..........",
		"xxx..x..........",
		"x.x..x..........",
		"x.x..xxxxxxx....",
		"................",
		"................",
		"................",
		"................",
	};
    
	static char *not_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		"......xxxxx.....",
		"....xx.....xx...",
		"...x.........x..",
		"..x........xx.x.",
		"..x......xx...x.",
		".x.....xx......x",
		".x...xx........x",
		"..xxx.........x.",
		"..x...........x.",
		"...x.........x..",
		"....xx.....xx...",
		"......xxxxx.....",
	};

	static char *empty_xpm[] = {
		"16 12 2 1",
		"x c #ffffff",
		". c #000000",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
		"................",
	};

	SDL_FillRect(m->screen, NULL, SDL_MapRGB( m->liveview->format, 0, 0, 0 ));
	sdl_paint_image(m, wait_xpm,  m->wx/2,     m->wy/2);
	sdl_paint_image(m, ok_xpm,    m->wx-m->bx/2,  m->wy-m->by/2);
	sdl_paint_image(m, exit_xpm,  m->bx/2,     m->wy-m->by/2);
	sdl_paint_image(m, f1m_xpm,   m->bx+m->sx/20, m->by/2);
	sdl_paint_image(m, f25cm_xpm, m->bx+m->sx/5,  m->by/2);

	sdl_paint_image(m, af_xpm,    m->bx/2,     m->by/2);
	if (!m->do_focus) {
		sdl_paint_image(m, not_xpm,  16+m->bx/2,     m->by/2);      
	}
	sdl_paint_image(m, ae_xpm,    m->wx-m->bx/2,  m->by/2);
	if (!m->do_exposure) {
		sdl_paint_image(m, not_xpm,  16+m->bx/2,     m->by/2);      
	}

#if 0
	sdl_paint_image(m, time_1_100_xpm, m->wx-m->bx/2, m->by+pix_exposure(10000));
	sdl_paint_image(m, time_1_10_xpm,  m->wx-m->bx/2, m->by+pix_exposure(100000));
	sdl_paint_image(m, iso400_xpm,     m->bx/2,    m->by+pix_gain(4.0));
#endif
		
	SDL_UpdateWindowSurface(m->window);
}

void fmt_print(struct v4l2_format *fmt)
{
	int f;
	printf("Format: %dx%d. ", fmt->fmt.pix.width, fmt->fmt.pix.height);
	printf("%x ", fmt->fmt.pix.pixelformat);
	f = fmt->fmt.pix.pixelformat;
	for (int i=0; i<4; i++) {
		printf("%c", f & 0xff);
		f >>= 8;
	}
	printf("\n");
}

pixel buf_pixel(struct v4l2_format *fmt, unsigned char *buf, int x, int y)
{
	pixel p = { 0, 0, 0, 0 };
	int pos = x + y*fmt->fmt.pix.width;
	int b;

	p.alpha = 128;

	switch (fmt->fmt.pix.pixelformat) {
	case '01AB': /* BA10, aka GRBG10, 
			https://www.linuxtv.org/downloads/v4l-dvb-apis-new/uapi/v4l/pixfmt-srggb10.html?highlight=ba10
		     */
		b = buf[pos*2];
		b += buf[pos*2+1] << 8;
		b /= 4;
		if ((y % 2) == 0) {
			switch (x % 2) {
			case 0: p.g = b/2; break;
			case 1: p.r = b; break;
			}
		} else {
			switch (x % 2) {
			case 0: p.b = b; break;
			case 1: p.g = b/2; break;
			}
		}
		break;

	case V4L2_PIX_FMT_RGB24:
		pos *= 4;
		p.r = buf[pos];
		p.g = buf[pos+1];
		p.b = buf[pos+2];
		break;

	default:
		printf("Wrong pixel format!\n");
		fmt_print(fmt);
		exit(1);
	}

	return p;
}

void sdl_render(struct sdl *m, unsigned char *buf, struct v4l2_format *fmt)
{
	if (!m->window) 
		return;
	sdl_begin_paint(m);    
	/* do your rendering here */

	for (int y = 0; y < m->sy; y++)
		for (int x = 0; x < m->sx; x++) {
			pixel p = buf_pixel(fmt, buf, x*m->factor, y*m->factor);
			p.alpha = 128;
			sfc_put_pixel(m->liveview, x, y, &p);
		}

	//render_statistics(liveview);

	sdl_finish_paint(m);
}

#if 0
void imageStatistics(FCam::Image img)
{
	int sx, sy;

	sx = img.size().width;                                                          
	sy = img.size().height;                                                         
	printf("Image is %d x %d, ", sx, sy);                                           

	printf("(%d) ", img.brightness(sx/2, sy/2));

	int dark = 0;                                                                   
	int bright = 0;                                                                 
	int total = 0;                                                                  
#define RATIO 10
	for (int y = sy/(2*RATIO); y < sy; y += sy/RATIO)                               
		for (int x = sx/(2*RATIO); x < sx; x += sx/RATIO) {                    
			int br = img.brightness(x, y);                               

			if (!d_raw) {
				/* It seems real range is 60 to 218 */
				if (br > 200)
					bright++;                                               
				if (br < 80)
					dark++;
			} else {
				/* there's a lot of noise, it seems black is commonly 65..71,
				   bright is cca 1023 */
				if (br > 1000)
					bright++;
				if (br < 75)
					dark++;
			}
			total++;                                                      
		}

	printf("%d dark %d bri,", dark, bright);                     

	long sharp = 0;
	for (int y = sy/3; y < 2*(sy/3); y+=sy/12) {
		int b = -1;
		for (int x = sx/3; x < 2*(sx/3); x++) {
			int b2;                                                                
			b2 = img.brightness(x, y/2);                                          
			if (b != -1)
				sharp += (b-b2) * (b-b2);                                              
			b = b2;                                                                
		}
	}
	printf("sh %d\n", sharp);
}
#endif


void sdl_init(struct sdl *m, int _sx, int _sy, int _factor)
{
	printf("Initing SDL\n");

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("Could not init SDL\n");
		exit(1);
	}

	atexit(SDL_Quit);

	m->wx = 800;
	m->wy = 400;

	m->window = SDL_CreateWindow( "Camera", SDL_WINDOWPOS_UNDEFINED,
				      SDL_WINDOWPOS_UNDEFINED, m->wx, m->wy,
				      SDL_WINDOW_SHOWN );
	if (m->window == NULL) {
		printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
	}

	m->screen = SDL_GetWindowSurface(m->window);
	if (!m->screen) {
		printf("Couldn't create liveview\n");
		exit(1);
	}

	m->sx = _sx;
	m->sy = _sy;
	m->factor = _factor;

	m->sx /= m->factor;
	m->sy /= m->factor;
    
	m->focus = 0;
	m->gain = 0;
	m->exposure = 0;

	m->bx = (m->wx-m->sx)/2;
	m->by = (m->wy-m->sy)/2;

	m->liveview = SDL_CreateRGBSurface(0,m->sx,m->sy,32,0,0,0,0);
	if (!m->liveview) {
		printf("Couldn't create liveview\n");
		exit(1);
	}
	sdl_paint_ui(m);
}
